<?php

/**
 * @file mcbacksync.module
 * TODO: Back Sync functionalities for Mailchimp
 */

define( 'MCBACKSYNC_ROLE_NEW_USER', 'normal' );
define( 'MCBACKSYNC_ROLE_SUPERADMIN', 'superadmin' );
define( 'MCBACKSYNC_ROLE_ADMIN', 'admin' );

/**
 * Implements hook_mailchimp_process_webhook()
 * Main rooting function of the module calling other functions depending on the 
 * type of webhook sent by Mailchimp.
 */
function mcbacksync_mailchimp_process_webhook($type, $data) {
  switch ( $type ) {
  case "subscribe":
    mcbacksync_webhook_mcsubscribe($data);
  break;
  case "upemail":
    mcbacksync_webhook_mcupemail($data);
  break;
  case "profile":
    mcbacksync_webhook_mcprofile($data);
  break;
  default: 
    
  }
}


/**
 * Implements hook_help().
 */
function mcbacksync_help($path, $arg) {
  switch ($path) {
    // Main module help for the block module
    case 'admin/help#block':
     return '<p>' . t('Solving missing sync funtionalities of the Mailchimp module, read the project page for more details:') . '<a href="https://www.drupal.org/project/mcbacksync" target="_blank">Mailchimp Back Sync project</a>' . '</p>';
  }
}

/**
 * Implements hook_menu 
 * TODO: Delete or comment the test item on final release.
 */

function mcbacksync_menu() {
  $items = array();
  
  $items['admin/config/services/mcbacksync'] = array(
    'title' => 'Mailchimp Back Sync module settings',
    'description' => 'Select the role(s) added when a Drupal user is created and the roles that can be affected by an email update.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mcbacksync_admin'),
    'access arguments' => array('administer mcbacksync settings'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
   );

   //TODO: Test Url, Delete or comment on final release.

  $items['mcbacksync/testurl'] = array(
    'title' => 'RAS MailChimp test webhooks endpoint',
    'page callback' => 'mcbacksync_test_url',
    'access arguments' => array('administer mailchimp'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function mcbacksync_permission() {
  return array(
    'administer mcbacksync' => array(
      'title' => t('administer mailchimp back sync'),
      'description' => t('Access the mcbacksync configuration options.'),
    ),
  );
}


/**
 * Used to test functions on a page... 
 * TODO: Delete or comment on final release.
 */
  function mcbacksync_test_url(){

    return array();
}

function mcbacksync_query_username( $name ) {
  $result = db_query("SELECT COUNT(*) FROM {users} WHERE name = :name;", array(':name' => $name))->fetchField();
  return $result;
} 

/**
 * Checks if the username exists already and return a non existing username 
 * by postpending an  increassing number at the end of it.
 */
function mcbacksync_valid_username( $name ) {
  $postpend = '';
  if ( $name === FALSE ) $name = 'mc';
  if ( mcbacksync_query_username( $name ) != 0 ) {
    $postpend = 1;
    do {
    $postpend++;
    }
    while ( mcbacksync_query_username( $name . $postpend ) != 0 );
  }
  return $name . $postpend;
}


/**
 * Returns an array of rid of roles having administrator permissions on the easy/best way possible.
 * Two of the admin roles names are defined on top of this file, the other is set up on the admin/config/people/accounts.
 * TODO: Give a configuration interface on backend to set the admin roles.
 */
function mcbacksync_get_not_allowed_roles() {
  $user_roles = user_roles(TRUE);
  $formated = array();

  $allowedroles = variable_get( 'mcbacksync_email_update' );
  foreach ( $allowedroles as $rid => $allowed ) {
    if ( $allowed > 0 ) unset( $user_roles[$rid] );
  }
  foreach ( $user_roles as $rid => $role ) {
    $formated[] = $rid;
  }
  return $formated;
}

/**
 * Retrieves the fields info linked with a mc_list_id.
 * This module could be required if the mailchimp subscription field of the Drupal 
 * users profile is not set as checked by default... 
 * TODO: Find the field by list_id and set the value on user creation.
 */
function mcbacksync_subscribe_field_from_listid($listid) {
  $mc_subscribe_fields = field_read_fields(array('type' => 'mailchimp_lists_subscription'));
  $filtered = array();
  foreach ( $mc_subscribe_fields as $field_name => $value ) {
    if ( $value['settings']['mc_list_id'] == $listid ) $filtered = $value;
  }
  return $filtered;
} 

/**
 * Get role object from it's name.
 */
function mcbacksync_get_role_by_name($name) {
  $roles = user_roles();
  return array_search($name, $roles);
}

/**
 * Check if the user is allowed to change it's email address.
 */
function mcbacksync_allow_email_change($account) {
  if ( $account->uid == 1 ) return FALSE;
  $admins = mcbacksync_get_not_allowed_roles();
  if ( count(array_intersect( $account->roles, $admins ) == 0 ) ) {
    // the user doesn't has one of the admin roles
    return TRUE;
  }
  return FALSE;
}

/**
 * Main function to treat the update of email reported from Mailchimp.
 */
function mcbacksync_webhook_mcupemail($data) { 
  if ( isset( $data['old_email'] ) && isset( $data['new_email'] ) ) {
    $account = user_load_by_mail( $data['old_email'] ) ;
    if ( $account !== FALSE && $account != NULL && is_object($account) &&  $account->uid > 1 && mcbacksync_allow_email_change($account) ) {
      // User found, change it's email address.
      $account = user_save($account, array( 'mail' =>  $data['new_email'] ) );
      if ( $account->mail === $data['new_email'] )  watchdog('mcbacksync', 'An email change request and executed. Old email: @old to new email: @new.',  array('@old' => $data['old_email'], '@new' => $data['new_email'] ), WATCHDOG_INFO);
      else watchdog('mcbacksync', 'An email change request but something went wrong... Old email: @old to new email: @new.',  array('@old' => $data['old_email'], '@new' => $data['new_email'] ), WATCHDOG_INFO);
    }
    else {
        watchdog('mcbacksync', 'An email change request was received from MC but the user doesn\'t exist or has admin role. Old email: @old to new email: @new.',  array('@old' => $data['old_email'], '@new' => $data['new_email'] ), WATCHDOG_ERROR);
        mcbacksync_webhook_mcsubscribe($data);
    }
  }
  else {
    watchdog('mcbacksync', 'MC upemail data not correctly structured. <pre>' . print_r( $data, -1) . '</pre>', array(), WATCHDOG_ERROR);
  }
}

/**
 * Main function to treat a profile update from Mailchimp.
 * TODO: if the mailchimp subscription field is attached to a node? use entities instead of user bundles.
 */
function mcbacksync_webhook_mcprofile($data) {
  watchdog('mcbacksync', 'Request of profile update has been requested: <pre> @data </pre>.', array('@data' => print_r($data, -1) ), WATCHDOG_INFO );
  if ( isset($data[ 'email' ]) && isset( $data['merges'] ) && count( $data['merges'] ) > 0 ) {
    $account = user_load_by_mail( $data['email'] ) ;

    if ( $account !== FALSE && is_object($account) && $account->uid > 1 ) {
      //retrieve field to subscribe to the list
      $field = mcbacksync_subscribe_field_from_listid($data['list_id']);

      // If the mailchimp subscription field is attached to an user.
      if ($fieldinfo = field_info_instance('user', $field['field_name'], 'user')) {
        // The custom field exists on the user object
        $edit = array();
        foreach ( $fieldinfo['settings']['mergefields'] as $mergename => $fieldmerge ) {
          //drupal_set_message('$fieldmerge: <pre>'.print_r($fieldmerge, -1).'</pre>');
          if ( isset( $data['merges'][$mergename] ) ) {
            $edit[$fieldmerge][LANGUAGE_NONE][0]['value'] = $data['merges'][$mergename];
          }
        }
     
      //avoid saving an account with an empty email when executed by mcbacksync_webhook_mcsubscribe()
      $edit['mail'] = $data['email'];
      $account = user_save($account, $edit);
      }
    }
    elseif ( $account == FALSE ) {
      // Modified profiel doesn't exist on Drupal so we create it as it was a new subscription.
      mcbacksync_webhook_mcsubscribe($data);
    }
      
  } 
  return TRUE;
}

/**
 * Main function to treat a subscription from Mailchimp.
 */
function mcbacksync_webhook_mcsubscribe($data) {
  //$subscribe_field = mcbacksync_subscribe_field_from_listid($data['list_id']);

  if ( isset( $data['email'] )) {
    $userexist = user_load_by_mail( $data['email'] ) ;
    if ( $userexist == FALSE  ) {
      $user = new stdClass;
      $password = user_password(8);
      $newrole = mcbacksync_get_role_by_name(MCBACKSYNC_ROLE_NEW_USER);

      $new_user_roles = array( );
      $configroles = variable_get( 'mcbacksync_newuser_roles' );
      foreach ( $configroles as $rid => $configrole ) {
        if ( $configrole > 0 ) $new_user_roles[$rid] = TRUE;
      }
      $name_mergetag = strtoupper( check_plain( variable_get('mcbacksync_newuser_name', FALSE) ) );
      if ( isset( $data['merges'][$name_mergetag] ) ) $name_from_mergetag = $data['merges'][$name_mergetag];
      else $name_from_mergetag = FALSE;
      $fields = array(
        'name' => mcbacksync_valid_username( $name_from_mergetag ),
        'mail' => $data['email'],
        'pass' => $password,
        'status' => 1,
        'init' => 'email address',
        'roles' => $new_user_roles
      );
       
      // if there is no field sent then the subscriptions are not shown correctly.
      $lists = mailchimp_get_lists();
      foreach ( $lists as $list ) { 
        $field_list = mcbacksync_subscribe_field_from_listid($list['id']);
        if ( isset ( $field_list['field_name'] ) ) {
          $fields[$field_list['field_name']] = array( 0 => array( 'value' => 'Dummy value') );
        }
      }

      $account = user_save('', $fields);
      $account->password = $fields['pass'];
      
      mcbacksync_webhook_mcprofile($data);

      // Log event:
      watchdog('mcbacksync', 'An user has been created: @email.',
        array('@email' => $data['email']), WATCHDOG_INFO
      );
    }
    elseif ( isset($userexist) && is_object($userexist)) {
      //Then we should update the Drupal user.
      watchdog('mcbacksync', 'An user subscribes but it exists already on the db: @email.',
        array('@email' => $data['email']), WATCHDOG_INFO
      );
      mcbacksync_webhook_mcprofile($data);
    }
    else { 
      watchdog('mcbacksync', 'An user subscribes but an unespected result happend : @email.',
        array('@email' => $data['email']), WATCHDOG_INFO
      );
      watchdog('mcbacksync', 'debug: <pre>' . print_r($userexist, -1) . '</pre>');
    }
  }
}

function mcbacksync_get_roles_options($values) {
  $options = user_roles(TRUE);
  unset( $options[variable_get('user_admin_role')]);
  return $options;
}

/**
 * Implements hook_field_prepare_view().
 *
 * Our field has no actual data in the database, so we have to push a dummy
 * value into $items, or the render system will assume we have nothing to
 * display. See https://api.drupal.org/comment/48043#comment-48043
 */
function mcbacksync_field_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items) {
  if ($field['type'] == 'mailchimp_lists_subscription') {
    foreach ($items as $key => $item) {
      drupal_set_message('<pre>'.print_r($item, -1).'</pre>');
      //$items[$key][0]['value'] = 'Dummy value';
    }
  }
}

function mcbacksync_admin() {
  $form = array();
  
  $form['mcbacksync_email_update'] = array(
    '#type' => 'checkboxes',
    '#id' => 'mcbacksync-email-update',
    '#title' => t('Update email on roles.'),
    '#options' => mcbacksync_get_roles_options( variable_get( 'mcbacksync_email_update' ) ),
    '#default_value' => variable_get('mcbacksync_email_update'),
    '#description' => t("The roles affected by the email update requests coming from Mailchimp. This avoid having an admin account changing it's email adress. User 1 is always blocked."),
    '#required' => FALSE,
  );
  $form['mcbacksync_newuser_roles'] = array(
    '#type' => 'checkboxes',
    '#id' => 'mcbacksync-newuser-roles',
    '#title' => t('Roles acquired on new accounts.'),
    '#options' => mcbacksync_get_roles_options( variable_get( 'mcbacksync_newuser_roles' ) ),
    '#default_value' => variable_get('mcbacksync_newuser_roles', FALSE),
    '#description' => t("The roles acquired when a user is created by this module."),
    '#required' => FALSE,
  );

  $form['mcbacksync_newuser_name'] = array(
    '#type' => 'textfield',
    '#id' => 'mcbacksync-newuser-name',
    '#title' => t('Mergetag used to create the user name on new accounts.'),
    '#size' => 40, 
    '#maxlength' => 40, 
    '#default_value' => variable_get('mcbacksync_newuser_name', FALSE),
    '#description' => t("Leave empty for a random generated user name."),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}
